﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Password_Keeper
{
    public partial class PasswordKeeperMainForm : Form
    {
        string UserName;                                           // имя пользователя
        string Password;                                           // пароль
        List<string> decryptedFile;                                // расшифрованный файл передаётся в прогу в виде коллекции строк
        KeyWordsFounder KWF;                                       // искатор ключевых слов
        public PasswordKeeperMainForm(string UserName, List<string> decryptedFile, string Password)
        {
            InitializeComponent();
            this.UserName = UserName;
            this.decryptedFile = decryptedFile;
            this.Password = Password;
        }

        private void PasswordKeeperMainForm_Load(object sender, EventArgs e)
        {
            this.Text = "Password Keeper - " + UserName + Password;                //получаем имя пользователя
            KWF = new KeyWordsFounder(decryptedFile);                   //искатор ключевых слов

            // всё что выше, инициализируется в глобальные для этой формы переменные, поэтому далее я буду к ним обращаться

            for (int i = 0; i < KWF.servFoundAndReturn().Count; i++)
            {
                ServicesList.Items.Add(KWF.servFoundAndReturn()[i]);        //выводим все сервисы пользователя в левый листбокс
            }
        }
        private void PasswordKeeperMainForm_Close(object sender, EventArgs e)
        {
            Encoder enc = new Encoder();
            if (File.Exists(UserName+".dat"))
                using (StreamWriter stream = new StreamWriter(UserName + ".dat"))
                    FullFill.WithInfo(stream, decryptedFile);





            Application.Exit();
        }

        private void ServicesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string s = ServicesList.Text;
            AccounsList.Items.Clear();
            for (int i = 0; i < KWF.accFoundAndReturn(s).Count; i++)
                AccounsList.Items.Add(KWF.accFoundAndReturn(s)[i]);

            if (AccounsList.Text == "")       // обработчик случая, если в сервисе нет аккаунтов, иначе будет показывать то, что было выбрано ранее
            {
                Acc_Login.Text = "Login";
                Acc_Password.Text = "Password";
                Acc_Comment.Text = "Комментарий...";
            }
        }

        private void AccounsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string serv = ServicesList.Text;
            string acc = AccounsList.Text;
            List<string> data = new List<string>();
            data = KWF.LogPassCommentFinder(serv, acc);
            Acc_Login.Text = data[0];
            Acc_Password.Text = data[1];
            Acc_Comment.Text = data[2];
        }

        private void LoginBufferCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(Acc_Login.Text);
        }

        private void PasswordBufferCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(Acc_Password.Text);
        }

        private void AddAccountButton_Click(object sender, EventArgs e)
        {
            Form f = new FormAddOrEditAccount(UserName, decryptedFile);
            f.Text = "Добавление аккаунта (" + UserName + ")";
            this.Hide();
            f.Show();

        }

        private void EditAccountButton_Click(object sender, EventArgs e)
        {
            if (AccounsList.SelectedItems.Count != 0)
            {
                Form f = new FormAddOrEditAccount(UserName, decryptedFile, ServicesList.Text, AccounsList.Text, Acc_Login.Text, Acc_Password.Text, Acc_Comment.Text);
                f.Text = "Редактирование аккаунта " + AccounsList.Text + " сервиса " + ServicesList.Text + " (" + UserName + ")";
                this.Hide();
                f.Show();
            }
            else
                MessageBox.Show("Не выбран аккаунт для редактирования", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }

        private void DeleteAccountButton_Click(object sender, EventArgs e)
        {
                if(MessageBox.Show("Вы действительно хотите удалить аккаунт " + AccounsList.Text + " сервиса " + ServicesList.Text + "?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    string acc = Convert.ToString(AccounsList.SelectedItem);
                    string serv = Convert.ToString(ServicesList.SelectedItem);
                    try
                    {
                        AccounsList.Items.RemoveAt(AccounsList.SelectedIndex);
                        KWF.DeleteAccount(acc, serv, decryptedFile);
                        if (AccounsList.Items.Count == 0)
                    {
                        ServicesList.Items.RemoveAt(ServicesList.SelectedIndex);
                        KWF.DeleteService(serv, decryptedFile);
                    }
                            
                    }
                    catch (IndexOutOfRangeException)
                    {
                        MessageBox.Show("Не выделено ни одного аккаунта", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }


                }
            }
            
            private void Accounts_add_Click(object sender, EventArgs e)
            {
                AddAccountButton_Click(sender, e);
            }

            private void Account_edit_Click(object sender, EventArgs e)
            {
                EditAccountButton_Click(sender, e);
            }

            private void Account_delete_Click(object sender, EventArgs e)
            {
                DeleteAccountButton_Click(sender, e);
            }

        private void Session_DeleteUser_Click(object sender, EventArgs e)
        {
           if(MessageBox.Show("Удалить все данные и файлы, связанные с пользователем " + UserName + "?", "Требуется подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)==DialogResult.Yes)
            {
                decryptedFile.Clear();
                File.Delete(UserName + ".dat");
                MessageBox.Show("Все данные удалены. Программа будет закрыта", "Успех", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Application.Exit();
            }
        }

        private void Session_closeprogram_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Session_end_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form f = new FormSignAndReg();
            f.Show();
        }
    }
}
