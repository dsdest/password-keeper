﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Password_Keeper
{
    static class passGenerator
    {
        public static StringBuilder Generate(TrackBar PassLengthBar, CheckBox RuleHasNumber, CheckBox RuleHasSymbol, CheckBox RuleHasUpperLetter)
        {
            int SymbAmount = PassLengthBar.Value;
            bool HasNumber = RuleHasNumber.Checked;
            bool HasSymbol = RuleHasSymbol.Checked;
            bool HasUpperLetter = RuleHasUpperLetter.Checked;
            StringBuilder password = new StringBuilder();

            var rand = new Random();

            //просто генерация пароля
            for(int i=0;i<SymbAmount;i++)
            {
                password.Insert(i, (char)rand.Next(33, 125));
            }

            // дополнение к паролю при наличии правила "есть число"
            if(HasNumber)
            {
                password.Insert(rand.Next(0, SymbAmount - 2), (char)rand.Next(48, 57));
                password.Remove(SymbAmount - 1, 1);
            }


            // дополнение к паролю если есть символ
            if (HasSymbol)
            {
                password.Insert(rand.Next(0, SymbAmount - 2), (char)rand.Next(33, 47));
                password.Remove(SymbAmount - 1, 1);
            }

            // дополнение к паролю если есть правило присутствия заглавной буквы
            if (HasUpperLetter)
            {
                password.Insert(rand.Next(0, SymbAmount - 2), (char)rand.Next(65, 90));
                password.Remove(SymbAmount - 1, 1);
            }


            return password;
        }
    }
}
