﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Password_Keeper
{
    public partial class RegForm : Form
    {
        private bool boxChecked = false;
        CryptedFileHandler handler;
        List<string> decryptedFile;
        public RegForm()
        {
            InitializeComponent();
        }

        private void NewPassword_TextChanged(object sender, EventArgs e)
        {
            if (NewPassword.Text != "Пароль")
            {
                NewPassword.PasswordChar = '*';
            }
        }
        private void NewPassword_MouseClick(object sender, EventArgs e)
        {
            if (NewPassword.Text == "Пароль")
            {
                NewPassword.Clear();
            }
            if (NewLogin.Text == "")
            {
                NewLogin.Text = "Логин";
            }
            if (NewPasswordRepeat.Text == "")
            {
                NewPasswordRepeat.PasswordChar = '\0';
                NewPasswordRepeat.Text = "Повторите пароль";
            }

        }

        private void NewPasswordRepeat_TextChanged(object sender, EventArgs e)
        {
            if (NewPasswordRepeat.Text != "Повторите пароль")
            {
                NewPasswordRepeat.PasswordChar = '*';
            }
        }

        private void NewLogin_MouseClick(object sender, EventArgs e)
        {
            if (NewLogin.Text == "Логин")
            {
                NewLogin.Clear();
            }
            if (NewPassword.Text == "")
            {
                NewPassword.PasswordChar = '\0';
                NewPassword.Text = "Пароль";
            }
            if (NewPasswordRepeat.Text == "")
            {
                NewPasswordRepeat.PasswordChar = '\0';
                NewPasswordRepeat.Text = "Повторите пароль";
            }
        }

        private void NewLPasswordRepeat_MouseClick(object sender, EventArgs e)
        {
            if (NewPasswordRepeat.Text == "Повторите пароль")
            {
                NewPasswordRepeat.Clear();
            }
            if (NewPassword.Text == "")
            {
                NewPassword.PasswordChar = '\0';
                NewPassword.Text = "Пароль";
            }
            if (NewLogin.Text == "")
            {
                NewLogin.Text = "Логин";
            }

        }

        private void NewRegButton_Click(object sender, EventArgs e)
        {
            RegValidator valid = new RegValidator();
            string state = valid.Validate(NewLogin.Text, NewPassword.Text, NewPasswordRepeat.Text);
            switch (state)
            {
                case "Пароли не равны":
                    {
                        MessageBox.Show("Введённый и повторно введённый пароли не совпадают. Проверьте правильность ввода паролей.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    }
                case "Аккаунт существует":
                    {
                        MessageBox.Show("Такой пользователь уже зарегистрирован. Для смены имени или пароля, зайдите в программу, используя свой логин и пароль и выберите «Текущая сессия», затем «Изменить имя или пароль текущего пользователя»", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    }
                case "Всё ОК":
                    {
                        MessageBox.Show("Файл аккаунта успешно создан.", "Успех", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        switch (GoToProgrammCheck.Checked)
                        {
                            case true:
                                {
                                    handler = new CryptedFileHandler(NewLogin.Text);        //подключается обработчик файла, ему в конструктор сразу передается адрес файла
                                    decryptedFile = handler.DecryptToStringArray();         //расшифровываем файл и получаем его в виде массива строк
                                    PasswordKeeperMainForm mainForm = new PasswordKeeperMainForm(NewLogin.Text, decryptedFile, NewPassword.Text);
                                    mainForm.Text += NewLogin.Text;
                                    mainForm.Show();
                                    boxChecked = true;
                                    this.Close();
                                    break;
                                }
                            case false:
                                {
                                    NewPassword.PasswordChar = '\0';
                                    NewPasswordRepeat.PasswordChar = '\0';
                                    NewLogin.Text = "Логин";
                                    NewPassword.Text = "Пароль";
                                    NewPasswordRepeat.Text = "Повторите пароль";
                                    
                                    break;
                                }
                        }
                        break;
                    }
            }


            
        }

        private void RegForm_Close(object sender, EventArgs e)
        {
            if (!boxChecked)
            {
                FormSignAndReg f = new FormSignAndReg();
                f.Show();
            }
        }
    }
}
