﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Password_Keeper
{
    public partial class FormSignAndReg : Form
    {
        CryptedFileHandler handler;
        List<string> decryptedFile=new List<string>();
        public FormSignAndReg()
        {
            InitializeComponent();
        }

        private void Login_MouseClick(object sender, EventArgs e)
        {
            if (Login.Text=="Логин")
            {
                Login.Clear();
            }
            if (Password.Text == "")
            {
                 Password.PasswordChar = '\0';
                 Password.Text = "Пароль";
            }
        }

        private void Password_MouseClick(object sender, EventArgs e)
        {
            if (Password.Text == "Пароль")
            {
                Password.Clear();
            }
            if (Login.Text == "")
            {
                Login.Text = "Логин";
            }
        }

        private void Password_TextChanged(object sender, EventArgs e)
        {
            if (Password.Text != "Пароль")
            {
                Password.PasswordChar='*';
            }
        }

        private void SignButton_Click(object sender, EventArgs e)
        {
            string acc = @Login.Text + ".dat";

            if (!File.Exists(acc)) MessageBox.Show("Такого аккаунта не существует. Проверьте правильность ввода данных. Если вы хотите зарегистрировать новый аккаунт, нажмите кнопку зарегистрироваться.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                LogPassCheck LogPassCombo = new LogPassCheck();
                if (!LogPassCombo.correct(acc, Login.Text, Password.Text))
                {
                    MessageBox.Show("Комбинация логин-пароль неверна. Проверьте правильность комбинации и повторите попытку.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    this.Hide();
                    handler = new CryptedFileHandler(Login.Text);       //подключается обработчик файла, ему в конструктор сразу передается адрес файла
                    decryptedFile = handler.DecryptToStringArray();     //расшифровываем файл и получаем его в виде коллекции строк
                    PasswordKeeperMainForm mainForm = new PasswordKeeperMainForm(Login.Text, decryptedFile, Password.Text);
                    mainForm.Text += Login.Text;
                    mainForm.Show();
                }
                
            }
        }

        private void RegButton_Click(object sender, EventArgs e)
        {
            RegForm regForm = new RegForm();
            regForm.Show();
            this.Hide();
        }

        private void FormSignAndReg_Close(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
