﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Password_Keeper
{
    class DepleteStringToParts
    {
        private string[] ret = new string[4];
        private string temp;
        public string[] Deplete(string s)
        {
            int i = s.IndexOf(';');
            if (s[i - 1] == ' ' && s[i + 1] == ' ')
            {
                ret[0] = s.Substring(0, i - 1); // нашли аккаунт
                temp = s.Substring(i + 2);  // забиваем временную строку всем остальным
            }
            i = temp.IndexOf(';');
            if (temp[i - 1] == ' ' && temp[i + 1] == ' ')
            {
                ret[1] = temp.Substring(0, i - 1); // нашли логин
                temp = temp.Substring(i + 2);  // забиваем временную строку всем остальным
            }
            i = temp.IndexOf(';');
            if (temp[i - 1] == ' ' && temp[i + 1] == ' ')
            {
                ret[2] = temp.Substring(0, i - 1); // нашли пароль
                ret[3] = temp.Substring(i + 2);  // остальное - комментарий
            }
            return ret;

        }
    }
}
