﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Password_Keeper
{
    public partial class FormAddOrEditAccount : Form
    {
        int strNum = 0;
        string UserName;                                    // имя пользователя
        List<string> decryptedFile;                         // расшифрованный файл передаётся в прогу в виде массива строк
        KeyWordsFounder KWF;                                // искатор ключевых слов
        List<string> Services = new List<string>();         // список сервисов
        bool createMode = true;                              // определяет, добавляется ли новый аккаунт (TRUE) или редактируется существующий(FALSE)
        // всё что касается добавления нового аккаунта
        public FormAddOrEditAccount(string UserName, List<string> decryptedFile)
        {
            InitializeComponent();
            AccConfirm.Text = "Подтвердить создание аккаунта";
            createMode = true;
            this.UserName = UserName;
            this.decryptedFile = decryptedFile;
            SymbAmount.Text = PassLengthBar.Value + " символов";
            KWF = new KeyWordsFounder(decryptedFile);
            Services = KWF.servFoundAndReturn();
            for (int i = 0; i < Services.Count; i++)
            {
                ServComboBox.Items.Add(Services[i]);
            }
            if (ServComboBox.Items.Count > 0) ServComboBox.SelectedIndex = 0;

        }
        private void FormAddOrEditAccount_Load(object sender, EventArgs e)
        {
            
        }
        private void PassLengthBar_Scroll(object sender, EventArgs e)
        {
            SymbAmount.Text = PassLengthBar.Value + " символов";
        }

        private void AddNewService_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            string a = Prompt.ShowDialog("Введите название сервиса", "Создание нового сервиса");
            bool fineToAdd = true;
            if (a != "")
            {
                for (int i = 0; i < Services.Count; i++)
                {
                    if (a == Services[i])
                    {
                        MessageBox.Show("Такой сервис уже имеется. Чтобы добавить новый аккаунт к имеющемуся сервису, выберите имеющийся сервис в выпадающем списке", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        fineToAdd = false;
                        break;
                    }
                }
                if (fineToAdd)
                {
                    Services.Insert(Services.Count, a);
                    ServComboBox.Items.Add(a);
                    ServComboBox.SelectedIndex = Services.Count - 1;
                }

            }
            this.Enabled = true;
        }

        private void AccConfirm_Click(object sender, EventArgs e)
        {
            const string keyWordDepleter = " ; ";
            bool isNewService = true;      // переменной будет присвоена ложь, если алгоритм ниже найдёт сервис в имеющихся
            // ищем, имеется ли уже такой сервис в какой-то строке
            // если имеется, записываем номер строки этого сервиса в переменную strNum
            if (Convert.ToString(ServComboBox.SelectedItem) != "")
            {
                for (int i = 0; i < decryptedFile.Count; i++)
                {
                    if (decryptedFile[i] == "serv : " + ServComboBox.Text)
                    {
                        strNum = i;
                        isNewService = false;
                        break;
                    }
                }
                // создание нового аккаунта
                if (createMode)
                {

                    if (!isNewService)
                    {
                        // просто добавляем новую строку в коллекцию с комбинацией слов
                        decryptedFile.Insert(strNum + 1, Acc_name.Text + keyWordDepleter + Acc_Login.Text + keyWordDepleter + Acc_Pass.Text + keyWordDepleter + Acc_commentary.Text);
                        MessageBox.Show("Новый аккаунт " + Acc_name.Text + " к сервису " + ServComboBox.Text + " добавлен успешно!", "Успех", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
                    else
                    {
                        //добавляем сначала строку с ключевым словом serv : 
                        //а уже потом комбинацию ключевых слов
                        decryptedFile.Add("serv : " + ServComboBox.Text);
                        decryptedFile.Add(Acc_name.Text + keyWordDepleter + Acc_Login.Text + keyWordDepleter + Acc_Pass.Text + keyWordDepleter + Acc_commentary.Text);
                        MessageBox.Show("Новый сервис " + ServComboBox.Text + " добавлен успешно\n" + "Новый аккаунт " + Acc_name.Text + " к сервису " + ServComboBox.Text + " добавлен успешно!", "Успех", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
                    // восстанавливаем всё по умолчанию
                    Acc_commentary.Text = "Комментарий...";
                    Acc_Login.Text = "Логин";
                    Acc_Pass.Text = "Пароль";
                    Acc_name.Text = "Название аккаунта";
                }
                // редактирование уже существующего аккаунта
                else
                {
                    for (int i = strNum; i < decryptedFile.Count; i++)
                    {
                        if (decryptedFile[i] == acc + keyWordDepleter + login + keyWordDepleter + pass + keyWordDepleter + comm)
                        {
                            decryptedFile[i] = Acc_name.Text + keyWordDepleter + Acc_Login.Text + keyWordDepleter + Acc_Pass.Text + keyWordDepleter + Acc_commentary.Text;
                            acc = Acc_name.Text;
                            login = Acc_Login.Text;
                            pass = Acc_Pass.Text;
                            comm = Acc_commentary.Text;
                            MessageBox.Show("Аккаунт " + Acc_name.Text + " отредактирован успешно!", "Успех", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                            break;
                        }
                    }
                }
                //сразу сохраняем все изменения в файл, используя заранее заготовленный класс
                CryptedFileHandler CFH = new CryptedFileHandler(UserName);
                CFH.EncryptToFile(decryptedFile);
            }
            else
            {
                MessageBox.Show("Укажите название сервиса", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
                
         
            
        }

        private void FormAddOrEditAccount_Close(object sender, EventArgs e)
        {
            Form f = new PasswordKeeperMainForm(UserName, decryptedFile, decryptedFile[1]);
            f.Show();
        }

        private void Acc_name_TextChanged(object sender, EventArgs e)
        {
            if (AutoComment.Checked)
            {
                Acc_commentary.Text = "Это аккаунт пользователя " + Acc_name.Text + " сервиса " + ServComboBox.Text + ".\nДата создания: " + DateTime.Now;
            }
        }

        private void ServComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AutoComment.Checked)
            {
                Acc_commentary.Text = "Это аккаунт пользователя " + Acc_name.Text + " сервиса " + ServComboBox.Text + ".\nДата создания: " + DateTime.Now;
            }
        }

        private void AutoComment_CheckedChanged(object sender, EventArgs e)
        {
            if (AutoComment.Checked)
            {
                Acc_commentary.ReadOnly = true;
                Acc_commentary.Text = "Это аккаунт пользователя " + Acc_name.Text + " сервиса " + ServComboBox.Text + ".\nДата создания: " + DateTime.Now;
            }
            else Acc_commentary.ReadOnly = false;
        }
        //всё что касается редакции уже существующего аккаунта
        string serv;
        string acc;
        string login;
        string pass;
        string comm;
        public FormAddOrEditAccount(string UserName, List<string> decryptedFile, string serv, string acc, string login, string pass, string comm)
        {
            InitializeComponent();
            AccConfirm.Text = "Подтвердить изменения в аккаунте";
            createMode = false;
            //запрещаем менять сервис при редактировании
            //потому что легче удалить старое и создать новое на новом сервисе
            ServComboBox.Enabled = false;
            AddNewService.Enabled = false;
            this.UserName = UserName;
            this.decryptedFile = decryptedFile;
            this.serv = serv;
            this.acc = acc;
            this.login = login;
            this.pass = pass;
            this.comm = comm;
            AutoComment.Checked = false;
            Acc_name.Text = acc;
            Acc_Login.Text = login;
            Acc_Pass.Text = pass;
            Acc_commentary.Text = comm;
            SymbAmount.Text = PassLengthBar.Value + " символов";
            KWF = new KeyWordsFounder(decryptedFile);
            Services = KWF.servFoundAndReturn();
            int boxDefaultIndex = 0;
            for (int i = 0; i < Services.Count; i++)
            {
                ServComboBox.Items.Add(Services[i]);
                if (Services[i] == serv) boxDefaultIndex = i;
            }
            ServComboBox.SelectedIndex = boxDefaultIndex;
            
        }

        private void PassGenerate_Click(object sender, EventArgs e)
        {
            textBox4.Text = Convert.ToString(passGenerator.Generate(PassLengthBar, RuleHasNumber, RuleHasSymbol, RuleHasUpperLetter));

        }

        private void PassCopy_Click(object sender, EventArgs e)
        {
            Acc_Pass.Text = textBox4.Text;
        }
    }
}