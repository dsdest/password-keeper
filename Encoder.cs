﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Password_Keeper
{
    class Encoder
    {
        public string Encode(string init)
        {
            int e;
            StringBuilder encoded=new StringBuilder();
            for(int i=0; i<init.Length; i++)
            {
                e = (int)System.Convert.ToChar(init[i]);
                e++;
                encoded.Append (Convert.ToChar(e));
            }
            return System.Convert.ToString(encoded);
        }

        public string Decode(string encoded)
        {
            int d;
            StringBuilder init = new StringBuilder();
            for(int i=0; i<encoded.Length; i++)
            {
                d = (int)System.Convert.ToChar(encoded[i]);
                d--;
                init.Append (Convert.ToChar(d));
            }
            return System.Convert.ToString(init);
        }
    }
}
