﻿using System;
using System.Windows.Forms;

namespace Password_Keeper
{
    partial class RegForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NewRegButton = new System.Windows.Forms.Button();
            this.NewLogin = new System.Windows.Forms.TextBox();
            this.NewPassword = new System.Windows.Forms.TextBox();
            this.NewPasswordRepeat = new System.Windows.Forms.TextBox();
            this.GoToProgrammCheck = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // NewRegButton
            // 
            this.NewRegButton.Location = new System.Drawing.Point(12, 143);
            this.NewRegButton.Name = "NewRegButton";
            this.NewRegButton.Size = new System.Drawing.Size(235, 58);
            this.NewRegButton.TabIndex = 0;
            this.NewRegButton.Text = "Создать пользователя";
            this.NewRegButton.UseVisualStyleBackColor = true;
            this.NewRegButton.Click += new System.EventHandler(this.NewRegButton_Click);
            // 
            // NewLogin
            // 
            this.NewLogin.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.NewLogin.Location = new System.Drawing.Point(12, 17);
            this.NewLogin.Name = "NewLogin";
            this.NewLogin.Size = new System.Drawing.Size(235, 20);
            this.NewLogin.TabIndex = 1;
            this.NewLogin.Text = "Логин";
            this.NewLogin.MouseClick += new System.Windows.Forms.MouseEventHandler(this.NewLogin_MouseClick);
            // 
            // NewPassword
            // 
            this.NewPassword.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.NewPassword.Location = new System.Drawing.Point(12, 43);
            this.NewPassword.Name = "NewPassword";
            this.NewPassword.Size = new System.Drawing.Size(235, 20);
            this.NewPassword.TabIndex = 2;
            this.NewPassword.Text = "Пароль";
            this.NewPassword.MouseClick += new System.Windows.Forms.MouseEventHandler(this.NewPassword_MouseClick);
            this.NewPassword.TextChanged += new System.EventHandler(this.NewPassword_TextChanged);
            // 
            // NewPasswordRepeat
            // 
            this.NewPasswordRepeat.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.NewPasswordRepeat.Location = new System.Drawing.Point(12, 69);
            this.NewPasswordRepeat.Name = "NewPasswordRepeat";
            this.NewPasswordRepeat.Size = new System.Drawing.Size(235, 20);
            this.NewPasswordRepeat.TabIndex = 3;
            this.NewPasswordRepeat.Text = "Повторите пароль";
            this.NewPasswordRepeat.MouseClick += new System.Windows.Forms.MouseEventHandler(this.NewLPasswordRepeat_MouseClick);
            this.NewPasswordRepeat.TextChanged += new System.EventHandler(this.NewPasswordRepeat_TextChanged);
            // 
            // GoToProgrammCheck
            // 
            this.GoToProgrammCheck.AutoSize = true;
            this.GoToProgrammCheck.Location = new System.Drawing.Point(12, 95);
            this.GoToProgrammCheck.Name = "GoToProgrammCheck";
            this.GoToProgrammCheck.Size = new System.Drawing.Size(248, 17);
            this.GoToProgrammCheck.TabIndex = 4;
            this.GoToProgrammCheck.Text = "Сразу зайти в программу с этими данными";
            this.GoToProgrammCheck.UseVisualStyleBackColor = true;
            // 
            // RegForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(264, 226);
            this.Controls.Add(this.GoToProgrammCheck);
            this.Controls.Add(this.NewPasswordRepeat);
            this.Controls.Add(this.NewPassword);
            this.Controls.Add(this.NewLogin);
            this.Controls.Add(this.NewRegButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "RegForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Регистрация";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RegForm_Close);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button NewRegButton;
        private System.Windows.Forms.TextBox NewLogin;
        private System.Windows.Forms.TextBox NewPassword;
        private System.Windows.Forms.TextBox NewPasswordRepeat;
        private System.Windows.Forms.CheckBox GoToProgrammCheck;
    }
}