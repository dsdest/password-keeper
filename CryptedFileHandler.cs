﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Password_Keeper
{
    class CryptedFileHandler
    {
        //этот класс должен уметь:
        //считывать и декодировать информацию с файла - ДА
        //кодировать и записывать информацию в файл - ДА
        //передавать программе необходимые ей данные в расшифрованном виде - ДА
        //забирать у программы данные, шифровать и помещать в файл - ДА

        //считывание происходит построчно в массив строк
        //первые 2 строки - логин и пароль соответственно
        //далее - сервис, обозначается в расшифрованном виде так:

        //serv : <название сервиса> (тут и далее без символов больше-меньше)

        //на следующей строчке - 4 значения через точку с запятой таким образом:

        //<имя> ; <логин> ; <пароль> ; <комментарий>
        //ПРОБЕЛЫ ВАЖНЫ!!!

        //и так несколько строк, сколько есть записей на данный сервис

        //потом другой сервис и всё по новой
        //ниже пример файла в расшифрованном виде


        /*
        User
        Password
        serv : ВКонтакте
        Иван Иванов ; +7955896348 ; somePassword ; Моя страница вконтакте
        Олег Петров ; +7486259874 ; anotherPassword ; Мой фейк ВК
        serv : Instagramm
        Настя Ивлеева ; nastyNastya ; secretIvleevasPassword ; Инстаграмм Настюхи Ивлеевой)))
         */

        //прога может ДОБАВЛЯТЬ новые сервисы и АККАУНТЫ к ним
        //надо постараться сделать чтоб была не перезапись файла, а всё таки его дополнение xD

        private string fPath;   // путь к файлу

        public CryptedFileHandler(string s) // конструктор сразу получает название профиля пользователя, делает из него путь к файлу и далее работает с ним
        {
            fPath = s + ".dat";
        }

        public List<string> DecryptToStringArray()    // расшифровка файла в коллекцию строк и передача коллекции программе
        {
            string line;
            int strAmount = 0;

            Encoder enc = new Encoder();    //заранее подрубается шифратор

            using (StreamReader accFile = new StreamReader(@fPath))
            {
                while ((line = accFile.ReadLine()) != null) // этим циклом считаем количество строк в файле, чтоб потом использовать это в цикле
                {
                    strAmount++;
                }

                List<string> fileContains = new List<string>();

                // возвращаемся в начало файла
                accFile.BaseStream.Seek(0, SeekOrigin.Begin);
                accFile.DiscardBufferedData();

                //заполняем массив строк расшифорованным файлом от начала и до конца
                for (int i = 0; i < strAmount; i++)
                {
                    fileContains.Add(enc.Decode(accFile.ReadLine()));
                }
                return fileContains;    //возвращаем расшифрованный файл в виде массива строк
            }
        }

        public void EncryptToFile(List<string> s)    // шифрование массива строк и засовывание его в файл
        {
            Encoder enc = new Encoder();    // подрубается шифратор

            using (StreamWriter accFile = new StreamWriter(fPath))
            {
                for (int i = 0; i < s.Count; i++) accFile.WriteLine(enc.Encode(s[i]));
            }
        }



    }
}

