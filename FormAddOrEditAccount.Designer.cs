﻿namespace Password_Keeper
{
    partial class FormAddOrEditAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ServComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.AddNewService = new System.Windows.Forms.Button();
            this.Acc_name = new System.Windows.Forms.TextBox();
            this.Acc_Login = new System.Windows.Forms.TextBox();
            this.Acc_Pass = new System.Windows.Forms.TextBox();
            this.AccConfirm = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.PassLengthBar = new System.Windows.Forms.TrackBar();
            this.SymbAmount = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.RuleHasUpperLetter = new System.Windows.Forms.CheckBox();
            this.RuleHasNumber = new System.Windows.Forms.CheckBox();
            this.RuleHasSymbol = new System.Windows.Forms.CheckBox();
            this.PassGenerate = new System.Windows.Forms.Button();
            this.PassCopy = new System.Windows.Forms.Button();
            this.Acc_commentary = new System.Windows.Forms.RichTextBox();
            this.AutoComment = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.PassLengthBar)).BeginInit();
            this.SuspendLayout();
            // 
            // ServComboBox
            // 
            this.ServComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ServComboBox.FormattingEnabled = true;
            this.ServComboBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ServComboBox.Location = new System.Drawing.Point(12, 25);
            this.ServComboBox.Name = "ServComboBox";
            this.ServComboBox.Size = new System.Drawing.Size(193, 21);
            this.ServComboBox.TabIndex = 0;
            this.ServComboBox.SelectedIndexChanged += new System.EventHandler(this.ServComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(196, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Выбрать из существующих сервисов";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(93, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "или";
            // 
            // AddNewService
            // 
            this.AddNewService.Location = new System.Drawing.Point(12, 65);
            this.AddNewService.Name = "AddNewService";
            this.AddNewService.Size = new System.Drawing.Size(193, 37);
            this.AddNewService.TabIndex = 3;
            this.AddNewService.Text = "Добавить новый сервис";
            this.AddNewService.UseVisualStyleBackColor = true;
            this.AddNewService.Click += new System.EventHandler(this.AddNewService_Click);
            // 
            // Acc_name
            // 
            this.Acc_name.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.Acc_name.Location = new System.Drawing.Point(12, 111);
            this.Acc_name.Name = "Acc_name";
            this.Acc_name.Size = new System.Drawing.Size(193, 20);
            this.Acc_name.TabIndex = 4;
            this.Acc_name.Text = "Название аккаунта";
            this.Acc_name.TextChanged += new System.EventHandler(this.Acc_name_TextChanged);
            // 
            // Acc_Login
            // 
            this.Acc_Login.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.Acc_Login.Location = new System.Drawing.Point(12, 137);
            this.Acc_Login.Name = "Acc_Login";
            this.Acc_Login.Size = new System.Drawing.Size(193, 20);
            this.Acc_Login.TabIndex = 5;
            this.Acc_Login.Text = "Логин";
            // 
            // Acc_Pass
            // 
            this.Acc_Pass.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.Acc_Pass.Location = new System.Drawing.Point(12, 163);
            this.Acc_Pass.Name = "Acc_Pass";
            this.Acc_Pass.Size = new System.Drawing.Size(193, 20);
            this.Acc_Pass.TabIndex = 6;
            this.Acc_Pass.Text = "Пароль";
            // 
            // AccConfirm
            // 
            this.AccConfirm.Location = new System.Drawing.Point(12, 199);
            this.AccConfirm.Name = "AccConfirm";
            this.AccConfirm.Size = new System.Drawing.Size(193, 37);
            this.AccConfirm.TabIndex = 7;
            this.AccConfirm.Text = "Подтвердить создание/изменение";
            this.AccConfirm.UseVisualStyleBackColor = true;
            this.AccConfirm.Click += new System.EventHandler(this.AccConfirm_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(404, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Генератор паролей";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(249, 25);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(396, 20);
            this.textBox4.TabIndex = 9;
            // 
            // PassLengthBar
            // 
            this.PassLengthBar.Location = new System.Drawing.Point(249, 57);
            this.PassLengthBar.Maximum = 30;
            this.PassLengthBar.Minimum = 5;
            this.PassLengthBar.Name = "PassLengthBar";
            this.PassLengthBar.Size = new System.Drawing.Size(307, 45);
            this.PassLengthBar.TabIndex = 15;
            this.PassLengthBar.Tag = "";
            this.PassLengthBar.Value = 5;
            this.PassLengthBar.Scroll += new System.EventHandler(this.PassLengthBar_Scroll);
            // 
            // SymbAmount
            // 
            this.SymbAmount.Location = new System.Drawing.Point(562, 57);
            this.SymbAmount.Name = "SymbAmount";
            this.SymbAmount.ReadOnly = true;
            this.SymbAmount.Size = new System.Drawing.Size(83, 20);
            this.SymbAmount.TabIndex = 16;
            this.SymbAmount.Text = "... символов";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(389, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Дополнительные правила";
            // 
            // RuleHasUpperLetter
            // 
            this.RuleHasUpperLetter.AutoSize = true;
            this.RuleHasUpperLetter.Location = new System.Drawing.Point(249, 160);
            this.RuleHasUpperLetter.Name = "RuleHasUpperLetter";
            this.RuleHasUpperLetter.Size = new System.Drawing.Size(215, 17);
            this.RuleHasUpperLetter.TabIndex = 18;
            this.RuleHasUpperLetter.Text = "Имеет хотя бы одну заглавную букву";
            this.RuleHasUpperLetter.UseVisualStyleBackColor = true;
            // 
            // RuleHasNumber
            // 
            this.RuleHasNumber.AutoSize = true;
            this.RuleHasNumber.Location = new System.Drawing.Point(249, 114);
            this.RuleHasNumber.Name = "RuleHasNumber";
            this.RuleHasNumber.Size = new System.Drawing.Size(161, 17);
            this.RuleHasNumber.TabIndex = 19;
            this.RuleHasNumber.Text = "Имеет хотя бы одну цифру";
            this.RuleHasNumber.UseVisualStyleBackColor = true;
            // 
            // RuleHasSymbol
            // 
            this.RuleHasSymbol.AutoSize = true;
            this.RuleHasSymbol.Location = new System.Drawing.Point(249, 137);
            this.RuleHasSymbol.Name = "RuleHasSymbol";
            this.RuleHasSymbol.Size = new System.Drawing.Size(169, 17);
            this.RuleHasSymbol.TabIndex = 20;
            this.RuleHasSymbol.Text = "Имеет хотя бы один символ";
            this.RuleHasSymbol.UseVisualStyleBackColor = true;
            // 
            // PassGenerate
            // 
            this.PassGenerate.Location = new System.Drawing.Point(249, 199);
            this.PassGenerate.Name = "PassGenerate";
            this.PassGenerate.Size = new System.Drawing.Size(195, 37);
            this.PassGenerate.TabIndex = 21;
            this.PassGenerate.Text = "Сгенерировать пароль";
            this.PassGenerate.UseVisualStyleBackColor = true;
            this.PassGenerate.Click += new System.EventHandler(this.PassGenerate_Click);
            // 
            // PassCopy
            // 
            this.PassCopy.Location = new System.Drawing.Point(450, 199);
            this.PassCopy.Name = "PassCopy";
            this.PassCopy.Size = new System.Drawing.Size(195, 37);
            this.PassCopy.TabIndex = 22;
            this.PassCopy.Text = "Поставить такой пароль";
            this.PassCopy.UseVisualStyleBackColor = true;
            this.PassCopy.Click += new System.EventHandler(this.PassCopy_Click);
            // 
            // Acc_commentary
            // 
            this.Acc_commentary.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.Acc_commentary.Location = new System.Drawing.Point(12, 256);
            this.Acc_commentary.Name = "Acc_commentary";
            this.Acc_commentary.Size = new System.Drawing.Size(432, 73);
            this.Acc_commentary.TabIndex = 23;
            this.Acc_commentary.Text = "Комментарий...";
            // 
            // AutoComment
            // 
            this.AutoComment.AutoSize = true;
            this.AutoComment.Checked = true;
            this.AutoComment.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AutoComment.Location = new System.Drawing.Point(450, 256);
            this.AutoComment.Margin = new System.Windows.Forms.Padding(1);
            this.AutoComment.Name = "AutoComment";
            this.AutoComment.Padding = new System.Windows.Forms.Padding(5);
            this.AutoComment.Size = new System.Drawing.Size(196, 27);
            this.AutoComment.TabIndex = 24;
            this.AutoComment.Text = "Автособираемый комментарий";
            this.AutoComment.UseVisualStyleBackColor = true;
            this.AutoComment.CheckedChanged += new System.EventHandler(this.AutoComment_CheckedChanged);
            // 
            // FormAddOrEditAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 346);
            this.Controls.Add(this.AutoComment);
            this.Controls.Add(this.Acc_commentary);
            this.Controls.Add(this.PassCopy);
            this.Controls.Add(this.PassGenerate);
            this.Controls.Add(this.RuleHasSymbol);
            this.Controls.Add(this.RuleHasNumber);
            this.Controls.Add(this.RuleHasUpperLetter);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.SymbAmount);
            this.Controls.Add(this.PassLengthBar);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.AccConfirm);
            this.Controls.Add(this.Acc_Pass);
            this.Controls.Add(this.Acc_Login);
            this.Controls.Add(this.Acc_name);
            this.Controls.Add(this.AddNewService);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ServComboBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "FormAddOrEditAccount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавить/редактировать аккаунт";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAddOrEditAccount_Close);
            this.Load += new System.EventHandler(this.FormAddOrEditAccount_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PassLengthBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ServComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button AddNewService;
        private System.Windows.Forms.TextBox Acc_name;
        private System.Windows.Forms.TextBox Acc_Login;
        private System.Windows.Forms.TextBox Acc_Pass;
        private System.Windows.Forms.Button AccConfirm;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TrackBar PassLengthBar;
        private System.Windows.Forms.TextBox SymbAmount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox RuleHasUpperLetter;
        private System.Windows.Forms.CheckBox RuleHasNumber;
        private System.Windows.Forms.CheckBox RuleHasSymbol;
        private System.Windows.Forms.Button PassGenerate;
        private System.Windows.Forms.Button PassCopy;
        private System.Windows.Forms.RichTextBox Acc_commentary;
        private System.Windows.Forms.CheckBox AutoComment;
    }
}