﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Password_Keeper
{
    static class FullFill
    {
        public static void WithInfo(StreamWriter stream, List<string> decryptedFile)
        {
            Encoder enc = new Encoder();
            foreach (string s in decryptedFile)
                stream.WriteLine(enc.Encode(s));
        }
    }
}
