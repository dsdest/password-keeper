﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Password_Keeper
{
    class KeyWordsFounder   // методы класса воспринимают и работают только с уже дешифрованным массивом строк!
    {
        private List<string> wholeString;   // вся коллекция строк, по которой потом будем искать ключевые слова
        private string accName;
        private string[] info;

        public KeyWordsFounder(List<string> s) // получаем строку
        {
            wholeString = s;
        }

        public List<string> servFoundAndReturn()   //находим все сервисы и возвращаем в виде массива строк список сервисов
        {
            List<string> servColl = new List<string>();
            int k = 0;
            for (int i = 0; i < wholeString.Count; i++)
            {
                if (wholeString[i].Contains("serv : "))
                {
                    servColl.Insert(k, wholeString[i].Replace("serv : ", ""));
                    k++;
                }
            }
            return servColl;
        }

        public List<string> accFoundAndReturn(string s)     // искатель строки, отвечающей за выделенный сервис. возвращает аккаунты сервиса
        {
            List<string> accColl = new List<string>();

            int beginAcc = 0;
            int endAcc = 0;     //номера начала и конца строк, содержащих инфу про аккаунты

            for (int i = 2; i < wholeString.Count; i++)  // ищем строку с этим сервисом
            {
                if (wholeString[i] == "serv : " + s)
                {
                    beginAcc = i + 1;     // номер первой строки с информацией об аккаунтах, тк она следующая после строки с сервисом
                    break;  // получаем номер строки и прерываем цикл, ведь мы считаем, что сервисы не повторяются
                }
            }

            for (int i = beginAcc; i < wholeString.Count; i++)     //ищем последний аккаунт соответствующего сервиса
            {
                if (wholeString[i].Contains("serv : "))
                {
                    endAcc = i - 1;   // номер последней строки, содержащей данные об этом аккаунте
                    break;
                }
                else if (i == (wholeString.Count - 1))
                {
                    endAcc = i;
                    break;
                }
            }
            int k = 0;
            int c;
            try
            {


                for (int i = beginAcc; i <= endAcc; i++)   // перебираем строки в границе опредененного диапазона, доходим до нужной строки и выводим данные об аккаунте
                {
                    c = wholeString[i].IndexOf(';');
                    if (wholeString[i][c - 1] == wholeString[i][c + 1] && wholeString[i][c - 1] == ' ')
                    {
                        accName = wholeString[i].Substring(0, c - 1);
                    }
                    accColl.Insert(k++, accName); // вставить в следующий элемент коллекции строчку, содержащую аккаунт
                }
            }
            catch(Exception)
            {

            }
            return accColl;
        }

        public List<string> LogPassCommentFinder(string serv, string acc)
        {
            List<string> Data = new List<string>();
            int beginAcc = 0;
            int endAcc = 0;     //номера начала и конца строк, содержащих инфу про аккаунты

            for (int i = 2; i < wholeString.Count; i++)  // ищем строку с этим сервисом
            {
                if (wholeString[i] == "serv : " + serv)
                {
                    beginAcc = i + 1;     // номер первой строки с информацией об аккаунтах, тк она следующая после строки с сервисом
                    break;  // получаем номер строки и прерываем цикл, ведь мы считаем, что сервисы не повторяются
                }
            }

            for (int i = beginAcc; i < wholeString.Count; i++)     //ищем последний аккаунт соответствующего сервиса
            {
                if (wholeString[i].Contains("serv : "))
                {
                    endAcc = i - 1;   // номер последней строки, содержащей данные об этом аккаунте
                    break;
                }
                else if (i == (wholeString.Count - 1))
                {
                    endAcc = i;
                    break;
                }
            }

            DepleteStringToParts depleter = new DepleteStringToParts();
            for (int i = beginAcc; i <= endAcc; i++)   // перебираем строки в границе опредененного диапазона, доходим до нужной строки и выводим данные об аккаунте
            {
                info = depleter.Deplete(wholeString[i]);
                if (info[0] == acc) break;
            }
            int k = 0;
            for (int i = 1; i < 4; i++)
            {
                Data.Insert(k++, info[i]);
            }
            return Data;
        }

        public void DeleteAccount(string acc, string serv, List<string> dectyptedFile)
        {
            int beginAcc = 0;
            int endAcc = 0;     //номера начала и конца строк, содержащих инфу про аккаунты

            for (int i = 2; i < dectyptedFile.Count; i++)  // ищем строку с этим сервисом
            {
                if (dectyptedFile[i] == "serv : " + serv)
                {
                    beginAcc = i + 1;     // номер первой строки с информацией об аккаунтах, тк она следующая после строки с сервисом
                    break;                // получаем номер строки и прерываем цикл, ведь мы считаем, что сервисы не повторяются
                }
            }

            for (int i = beginAcc; i < dectyptedFile.Count; i++)     //ищем последний аккаунт соответствующего сервиса
            {
                if (dectyptedFile[i].Contains("serv : "))
                {
                    endAcc = i - 1;   // номер последней строки, содержащей данные об этом аккаунте
                    break;
                }
                else if (i == (wholeString.Count - 1))
                {
                    endAcc = i;
                    break;
                }
            }

            for (int i = beginAcc; i <= endAcc; i++)   // перебираем строки в границе опредененного диапазона, доходим до нужной строки и удаляем данные об аккаунте
            {
                if (dectyptedFile[i].Contains(acc))
                    dectyptedFile.RemoveAt(i);
            }
        }
        public void DeleteService(string serv, List<string> dectyptedFile)
        {
            for (int i = 2; i < dectyptedFile.Count; i++)  // ищем строку с этим сервисом
            {
                if (dectyptedFile[i] == "serv : " + serv)
                {
                    dectyptedFile.RemoveAt(i);
                    break;                // получаем номер строки и прерываем цикл, ведь мы считаем, что сервисы не повторяются
                }
            }
        }
    }
}