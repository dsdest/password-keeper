﻿namespace Password_Keeper
{
    partial class FormSignAndReg
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Login = new System.Windows.Forms.TextBox();
            this.Password = new System.Windows.Forms.TextBox();
            this.SignButton = new System.Windows.Forms.Button();
            this.RegButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Login
            // 
            this.Login.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.Login.Location = new System.Drawing.Point(12, 24);
            this.Login.Name = "Login";
            this.Login.Size = new System.Drawing.Size(286, 20);
            this.Login.TabIndex = 0;
            this.Login.Text = "Логин";
            this.Login.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Login_MouseClick);
            // 
            // Password
            // 
            this.Password.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.Password.Location = new System.Drawing.Point(12, 50);
            this.Password.Name = "Password";
            this.Password.Size = new System.Drawing.Size(286, 20);
            this.Password.TabIndex = 1;
            this.Password.Text = "Пароль";
            this.Password.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Password_MouseClick);
            this.Password.TextChanged += new System.EventHandler(this.Password_TextChanged);
            // 
            // SignButton
            // 
            this.SignButton.Location = new System.Drawing.Point(12, 76);
            this.SignButton.Name = "SignButton";
            this.SignButton.Size = new System.Drawing.Size(286, 46);
            this.SignButton.TabIndex = 2;
            this.SignButton.Text = "Войти";
            this.SignButton.UseVisualStyleBackColor = true;
            this.SignButton.Click += new System.EventHandler(this.SignButton_Click);
            // 
            // RegButton
            // 
            this.RegButton.Location = new System.Drawing.Point(12, 154);
            this.RegButton.Name = "RegButton";
            this.RegButton.Size = new System.Drawing.Size(286, 46);
            this.RegButton.TabIndex = 3;
            this.RegButton.Text = "Зарегистрироваться";
            this.RegButton.UseVisualStyleBackColor = true;
            this.RegButton.Click += new System.EventHandler(this.RegButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 138);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Ещё не являетесь пользователем?";
            // 
            // FormSignAndReg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(310, 226);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RegButton);
            this.Controls.Add(this.SignButton);
            this.Controls.Add(this.Password);
            this.Controls.Add(this.Login);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "FormSignAndReg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Вход и регистрация";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSignAndReg_Close);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Login;
        private System.Windows.Forms.TextBox Password;
        private System.Windows.Forms.Button SignButton;
        private System.Windows.Forms.Button RegButton;
        private System.Windows.Forms.Label label1;
    }
}

