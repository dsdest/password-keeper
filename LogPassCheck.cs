﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Password_Keeper
{
    class LogPassCheck
    {
        public bool correct (string @fPath, string login, string password)
        {
            Encoder enc = new Encoder();
            string log, pass;
            using (StreamReader fileAcc = new StreamReader(fPath))
            {
                log = enc.Decode(fileAcc.ReadLine());
                pass = enc.Decode(fileAcc.ReadLine());
            }
            if (log == login && pass == password) return true;
            else return false;
        }
    }
}
