﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Password_Keeper
{
    class RegValidator
    {
        public string Validate(string login, string password, string RepeatPassword)
        {
            if (password != RepeatPassword) return "Пароли не равны";

            else
            {
                string filePath = @login + ".dat";
                if (File.Exists(filePath)) return "Аккаунт существует";

                else
                {
                    Encoder enc = new Encoder();
                    using (StreamWriter fileAcc = new StreamWriter(filePath, false, System.Text.Encoding.Default))
                    {
                        fileAcc.WriteLine(enc.Encode(login));
                        fileAcc.WriteLine(enc.Encode(password));
                    }
                    return "Всё ОК";
                }
            }
        }
    }
}
