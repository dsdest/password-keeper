﻿namespace Password_Keeper
{
    partial class PasswordKeeperMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ServicesList = new System.Windows.Forms.ListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.Session_Param = new System.Windows.Forms.ToolStripMenuItem();
            this.Session_changeData = new System.Windows.Forms.ToolStripMenuItem();
            this.Session_DeleteUser = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.Session_end = new System.Windows.Forms.ToolStripMenuItem();
            this.Session_closeprogram = new System.Windows.Forms.ToolStripMenuItem();
            this.Accounts_ = new System.Windows.Forms.ToolStripMenuItem();
            this.Accounts_add = new System.Windows.Forms.ToolStripMenuItem();
            this.Account_edit = new System.Windows.Forms.ToolStripMenuItem();
            this.Account_delete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.Accounts_Statement = new System.Windows.Forms.ToolStripMenuItem();
            this.Info = new System.Windows.Forms.ToolStripMenuItem();
            this.Info_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.Info_About = new System.Windows.Forms.ToolStripMenuItem();
            this.Info_Site = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.ServiceFilter = new System.Windows.Forms.TextBox();
            this.AccountsFilter = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.AccounsList = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Acc_Login = new System.Windows.Forms.TextBox();
            this.Acc_Password = new System.Windows.Forms.TextBox();
            this.Acc_Comment = new System.Windows.Forms.RichTextBox();
            this.LoginBufferCopy = new System.Windows.Forms.Button();
            this.PasswordBufferCopy = new System.Windows.Forms.Button();
            this.AddAccountButton = new System.Windows.Forms.Button();
            this.DeleteAccountButton = new System.Windows.Forms.Button();
            this.EditAccountButton = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ServicesList
            // 
            this.ServicesList.FormattingEnabled = true;
            this.ServicesList.Location = new System.Drawing.Point(12, 76);
            this.ServicesList.Name = "ServicesList";
            this.ServicesList.Size = new System.Drawing.Size(204, 355);
            this.ServicesList.TabIndex = 0;
            this.ServicesList.SelectedIndexChanged += new System.EventHandler(this.ServicesList_SelectedIndexChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Session_Param,
            this.Accounts_,
            this.Info});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(943, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // Session_Param
            // 
            this.Session_Param.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Session_changeData,
            this.Session_DeleteUser,
            this.toolStripSeparator1,
            this.Session_end,
            this.Session_closeprogram});
            this.Session_Param.Name = "Session_Param";
            this.Session_Param.Size = new System.Drawing.Size(106, 20);
            this.Session_Param.Text = "Текущая сессия";
            // 
            // Session_changeData
            // 
            this.Session_changeData.Name = "Session_changeData";
            this.Session_changeData.Size = new System.Drawing.Size(452, 22);
            this.Session_changeData.Text = "Изменить имя или пароль текущего пользователя";
            // 
            // Session_DeleteUser
            // 
            this.Session_DeleteUser.Name = "Session_DeleteUser";
            this.Session_DeleteUser.Size = new System.Drawing.Size(452, 22);
            this.Session_DeleteUser.Text = "Удалить все данные текущего пользователя";
            this.Session_DeleteUser.Click += new System.EventHandler(this.Session_DeleteUser_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(449, 6);
            // 
            // Session_end
            // 
            this.Session_end.Name = "Session_end";
            this.Session_end.Size = new System.Drawing.Size(452, 22);
            this.Session_end.Text = "Завершить текущую сессию и выйти в режим выбора пользователя";
            this.Session_end.Click += new System.EventHandler(this.Session_end_Click);
            // 
            // Session_closeprogram
            // 
            this.Session_closeprogram.Name = "Session_closeprogram";
            this.Session_closeprogram.Size = new System.Drawing.Size(452, 22);
            this.Session_closeprogram.Text = "Завершить текущую сессию и выйти из программы";
            this.Session_closeprogram.Click += new System.EventHandler(this.Session_closeprogram_Click);
            // 
            // Accounts_
            // 
            this.Accounts_.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Accounts_add,
            this.Account_edit,
            this.Account_delete,
            this.toolStripSeparator2,
            this.Accounts_Statement});
            this.Accounts_.Name = "Accounts_";
            this.Accounts_.Size = new System.Drawing.Size(88, 20);
            this.Accounts_.Text = "Ваш аккаунт";
            // 
            // Accounts_add
            // 
            this.Accounts_add.Name = "Accounts_add";
            this.Accounts_add.Size = new System.Drawing.Size(455, 22);
            this.Accounts_add.Text = "Добавить аккаунт";
            this.Accounts_add.Click += new System.EventHandler(this.Accounts_add_Click);
            // 
            // Account_edit
            // 
            this.Account_edit.Name = "Account_edit";
            this.Account_edit.Size = new System.Drawing.Size(455, 22);
            this.Account_edit.Text = "Редактировать выбранный аккаунт";
            this.Account_edit.Click += new System.EventHandler(this.Account_edit_Click);
            // 
            // Account_delete
            // 
            this.Account_delete.Name = "Account_delete";
            this.Account_delete.Size = new System.Drawing.Size(455, 22);
            this.Account_delete.Text = "Удалить выбранный аккаунт";
            this.Account_delete.Click += new System.EventHandler(this.Account_delete_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(452, 6);
            // 
            // Accounts_Statement
            // 
            this.Accounts_Statement.Name = "Accounts_Statement";
            this.Accounts_Statement.Size = new System.Drawing.Size(455, 22);
            this.Accounts_Statement.Text = "Создать выписку данных обо всех аккаунтах пользователя в txt файл";
            // 
            // Info
            // 
            this.Info.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Info_Help,
            this.Info_About,
            this.Info_Site});
            this.Info.Name = "Info";
            this.Info.Size = new System.Drawing.Size(93, 20);
            this.Info.Text = "Информация";
            // 
            // Info_Help
            // 
            this.Info_Help.Name = "Info_Help";
            this.Info_Help.Size = new System.Drawing.Size(169, 22);
            this.Info_Help.Text = "Помощь и FAQ";
            // 
            // Info_About
            // 
            this.Info_About.Name = "Info_About";
            this.Info_About.Size = new System.Drawing.Size(169, 22);
            this.Info_About.Text = "О программе";
            // 
            // Info_Site
            // 
            this.Info_Site.Name = "Info_Site";
            this.Info_Site.Size = new System.Drawing.Size(169, 22);
            this.Info_Site.Text = "Сайт программы";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Сервисы";
            // 
            // ServiceFilter
            // 
            this.ServiceFilter.Location = new System.Drawing.Point(12, 53);
            this.ServiceFilter.Name = "ServiceFilter";
            this.ServiceFilter.ReadOnly = true;
            this.ServiceFilter.Size = new System.Drawing.Size(204, 20);
            this.ServiceFilter.TabIndex = 3;
            this.ServiceFilter.Text = "Фильтр (пока в разработке)";
            // 
            // AccountsFilter
            // 
            this.AccountsFilter.Location = new System.Drawing.Point(248, 53);
            this.AccountsFilter.Name = "AccountsFilter";
            this.AccountsFilter.ReadOnly = true;
            this.AccountsFilter.Size = new System.Drawing.Size(204, 20);
            this.AccountsFilter.TabIndex = 6;
            this.AccountsFilter.Text = "Фильтр (пока в разработке)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(245, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Аккаунты";
            // 
            // AccounsList
            // 
            this.AccounsList.FormattingEnabled = true;
            this.AccounsList.HorizontalScrollbar = true;
            this.AccounsList.Location = new System.Drawing.Point(248, 76);
            this.AccounsList.Name = "AccounsList";
            this.AccounsList.Size = new System.Drawing.Size(204, 355);
            this.AccounsList.TabIndex = 4;
            this.AccounsList.SelectedIndexChanged += new System.EventHandler(this.AccounsList_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(477, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Информация об аккаунте";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(477, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Логин";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(477, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Пароль";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(477, 167);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Комментарий";
            // 
            // Acc_Login
            // 
            this.Acc_Login.Location = new System.Drawing.Point(528, 73);
            this.Acc_Login.Name = "Acc_Login";
            this.Acc_Login.ReadOnly = true;
            this.Acc_Login.Size = new System.Drawing.Size(159, 20);
            this.Acc_Login.TabIndex = 11;
            this.Acc_Login.Text = "Login";
            // 
            // Acc_Password
            // 
            this.Acc_Password.Location = new System.Drawing.Point(528, 108);
            this.Acc_Password.Name = "Acc_Password";
            this.Acc_Password.ReadOnly = true;
            this.Acc_Password.Size = new System.Drawing.Size(159, 20);
            this.Acc_Password.TabIndex = 12;
            this.Acc_Password.Text = "Password";
            // 
            // Acc_Comment
            // 
            this.Acc_Comment.Location = new System.Drawing.Point(480, 200);
            this.Acc_Comment.Name = "Acc_Comment";
            this.Acc_Comment.ReadOnly = true;
            this.Acc_Comment.Size = new System.Drawing.Size(413, 63);
            this.Acc_Comment.TabIndex = 13;
            this.Acc_Comment.Text = "Комментарий...";
            // 
            // LoginBufferCopy
            // 
            this.LoginBufferCopy.Location = new System.Drawing.Point(693, 67);
            this.LoginBufferCopy.Name = "LoginBufferCopy";
            this.LoginBufferCopy.Size = new System.Drawing.Size(200, 30);
            this.LoginBufferCopy.TabIndex = 14;
            this.LoginBufferCopy.Text = "Копировать в буфер обмена";
            this.LoginBufferCopy.UseVisualStyleBackColor = true;
            this.LoginBufferCopy.Click += new System.EventHandler(this.LoginBufferCopy_Click);
            // 
            // PasswordBufferCopy
            // 
            this.PasswordBufferCopy.Location = new System.Drawing.Point(693, 103);
            this.PasswordBufferCopy.Name = "PasswordBufferCopy";
            this.PasswordBufferCopy.Size = new System.Drawing.Size(200, 30);
            this.PasswordBufferCopy.TabIndex = 15;
            this.PasswordBufferCopy.Text = "Копировать в буфер обмена";
            this.PasswordBufferCopy.UseVisualStyleBackColor = true;
            this.PasswordBufferCopy.Click += new System.EventHandler(this.PasswordBufferCopy_Click);
            // 
            // AddAccountButton
            // 
            this.AddAccountButton.Location = new System.Drawing.Point(480, 269);
            this.AddAccountButton.Name = "AddAccountButton";
            this.AddAccountButton.Size = new System.Drawing.Size(412, 50);
            this.AddAccountButton.TabIndex = 16;
            this.AddAccountButton.Text = "Добавить аккаунт";
            this.AddAccountButton.UseVisualStyleBackColor = true;
            this.AddAccountButton.Click += new System.EventHandler(this.AddAccountButton_Click);
            // 
            // DeleteAccountButton
            // 
            this.DeleteAccountButton.Location = new System.Drawing.Point(480, 381);
            this.DeleteAccountButton.Name = "DeleteAccountButton";
            this.DeleteAccountButton.Size = new System.Drawing.Size(412, 50);
            this.DeleteAccountButton.TabIndex = 17;
            this.DeleteAccountButton.Text = "Удалить аккаунт";
            this.DeleteAccountButton.UseVisualStyleBackColor = true;
            this.DeleteAccountButton.Click += new System.EventHandler(this.DeleteAccountButton_Click);
            // 
            // EditAccountButton
            // 
            this.EditAccountButton.Location = new System.Drawing.Point(480, 325);
            this.EditAccountButton.Name = "EditAccountButton";
            this.EditAccountButton.Size = new System.Drawing.Size(412, 50);
            this.EditAccountButton.TabIndex = 18;
            this.EditAccountButton.Text = "Редактировать аккаунт";
            this.EditAccountButton.UseVisualStyleBackColor = true;
            this.EditAccountButton.Click += new System.EventHandler(this.EditAccountButton_Click);
            // 
            // PasswordKeeperMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(943, 462);
            this.Controls.Add(this.EditAccountButton);
            this.Controls.Add(this.DeleteAccountButton);
            this.Controls.Add(this.AddAccountButton);
            this.Controls.Add(this.PasswordBufferCopy);
            this.Controls.Add(this.LoginBufferCopy);
            this.Controls.Add(this.Acc_Comment);
            this.Controls.Add(this.Acc_Password);
            this.Controls.Add(this.Acc_Login);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.AccountsFilter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.AccounsList);
            this.Controls.Add(this.ServiceFilter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ServicesList);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "PasswordKeeperMainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Password Keeper - ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PasswordKeeperMainForm_Close);
            this.Load += new System.EventHandler(this.PasswordKeeperMainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox ServicesList;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Session_Param;
        private System.Windows.Forms.ToolStripMenuItem Session_changeData;
        private System.Windows.Forms.ToolStripMenuItem Accounts_;
        private System.Windows.Forms.ToolStripMenuItem Session_DeleteUser;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem Session_end;
        private System.Windows.Forms.ToolStripMenuItem Session_closeprogram;
        private System.Windows.Forms.ToolStripMenuItem Accounts_add;
        private System.Windows.Forms.ToolStripMenuItem Account_edit;
        private System.Windows.Forms.ToolStripMenuItem Account_delete;
        private System.Windows.Forms.ToolStripMenuItem Info;
        private System.Windows.Forms.ToolStripMenuItem Info_Help;
        private System.Windows.Forms.ToolStripMenuItem Info_About;
        private System.Windows.Forms.ToolStripMenuItem Info_Site;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ServiceFilter;
        private System.Windows.Forms.TextBox AccountsFilter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox AccounsList;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem Accounts_Statement;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Acc_Login;
        private System.Windows.Forms.TextBox Acc_Password;
        private System.Windows.Forms.RichTextBox Acc_Comment;
        private System.Windows.Forms.Button LoginBufferCopy;
        private System.Windows.Forms.Button PasswordBufferCopy;
        private System.Windows.Forms.Button AddAccountButton;
        private System.Windows.Forms.Button DeleteAccountButton;
        private System.Windows.Forms.Button EditAccountButton;
    }
}